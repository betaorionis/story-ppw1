from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='profil'),
    path('games', views.games, name='games'),
    path('time/<int:additional>', views.time, name='time'),
    path('time', views.time, name='time'),
]
