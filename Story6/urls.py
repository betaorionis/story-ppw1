from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.kegiatan, name='kegiatan'),
    path('tambahKegiatan/', views.tambah_kegiatan, name='tambahKegiatan'),
    path('daftarKegiatan/<int:index>/', views.tambah_orang, name='tambahOrang'),
]
