from django.shortcuts import render
from .models import Kegiatan, Person
from Story6 import forms, models
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

# Create your views here.

def kegiatan(request):
    kegiatan = models.Kegiatan.objects.all().values()
    orang = models.Person.objects.all().values()
    return render(request, "kegiatan.html", {"kegiatan": kegiatan, "orang": orang})

def tambah_kegiatan(request):
    if request.method == "POST":

        form = forms.kegiatanForm(request.POST)

        if 'id' in request.POST:
            Kegiatan.objects.get(id=request.POST['id']).delete()

            return redirect('/kegiatan')

        if form.is_valid():
            form.save()

            return redirect('/kegiatan')

    else:
        form = forms.kegiatanForm()

    return render(request, 'tambahKegiatan.html', {'form': form})

def tambah_orang(request, index):
    if request.method == "POST":
        form = forms.orangForm(request.POST)

        if form.is_valid():
            orang = Person(namaKegiatan=Kegiatan.objects.get(id=index), namaOrang=form.data['namaOrang'])
            print(orang.namaKegiatan.id)
            print()
            return redirect('/kegiatan')

    else:
        form = forms.orangForm()

    return render(request, 'tambahOrang.html', {'form': form})