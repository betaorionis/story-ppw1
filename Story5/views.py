from django.shortcuts import render
from django.shortcuts import render, redirect
from .forms import MatkulForm
from django.http import HttpResponseRedirect
from datetime import datetime
from .models import Matkul

# Create your views here.

def matkul(request):
    if request.method == "POST":
        form = MatkulForm(request.POST)

        if 'id' in request.POST:
            Matkul.objects.get(id=request.POST['id']).delete()

            return redirect('/matkul')

        if form.is_valid():
            matkul = Matkul(
                nama = form.data['nama'],
                kelas = form.data['kelas'],
                dosen = form.data['dosen'],
                sks = form.data['sks'],
                smt = form.data['smt'] + " " + form.data['tahun'],
                )
            matkul.save()

            return redirect('/matkul')

    else:

        form = MatkulForm()

    context = {
        'context' : {'now': datetime.now()},
        'matkul': Matkul.objects.all().values(),
        'form' : form,
        }
    for i in context['matkul']:
        i['id'] = "/matkul/{}".format(i['id'])

    return render(request, 'matkul.html', context)

def details(request, id):
    matkul_details =  Matkul.objects.get(pk=id)
    dict_matkul = {
        'details' : Matkul.objects.filter(id=id).values()
    }
    

    return render(request, "details.html", dict_matkul)
