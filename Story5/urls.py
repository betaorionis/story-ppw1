from django.urls import path, include
from . import views

urlpatterns = [
    path('matkul', views.matkul, name='matkul'),
    path('matkul/<int:id>', views.details, name='id'),
]
